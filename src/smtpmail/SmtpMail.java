/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smtpmail;


import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Authenticator;
import javax.mail.BodyPart;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;


/**
 *
 * @author cemb
 */
public class SmtpMail {
    
    public static String CONFIGURATION = "email_configurations";
    
    public static String SMPT_HOST_COMMENT = "SMTP Host Ip Address or Domain";
    public static String SMPT_HOST = "smtp_host";
    
    public static String USER_NAME_COMMENT = "SMTP User Name";
    public static String USER_NAME = "smtp_user";
    
    public static String PASSWORD_COMMENT = "SMTP User Password";
    public static String PASSWORD = "smtp_password";
    
    public static String PORT_NUMBER_COMMENT = "SMTP Port Number";
    public static String PORT_NUMBER = "smtp_port";
    
    public static String IS_SMPT_AUTH_ENEBLED_COMMENT = "SMTP Authentication [true or false]";
    public static String IS_SMPT_AUTH_ENEBLED = "smtp_auth";
    
    public static String ENCRYPTION_COMMENT = "Encryption[none or TLS or SSL]";
    public static String ENCRYPTION = "encryption";
    
    public static String FROM_ADDESS_COMMENT = "E-mail Address that will send e-mails";
    public static String FROM_ADDESS = "from_email_address";
        
    private String host;
    private int port;
    private String user;
    private String pass;
    private boolean authEnabled;
    private String fromAddress;
    private String encrypt;

    private final boolean kerberos = "true".equals(System.getProperty("sun.security.krb5", "false"));
    private Session session = null;
    
    public static void main(String[] args) throws MessagingException, ParserConfigurationException, IOException, SAXException {    
       
        SmtpMail mail = new SmtpMail();
       
        mail.SendAlertMails("cem.bayraktutan@dva.com.tr,hazar.ay@dva.com.tr","test multi","test multi");
        
    } 
  
    public void createSession() throws RuntimeException, MessagingException {
        
        Properties properties = new Properties();
    
        final String username = this.kerberos ? "dummyUser" : this.user;
        final String password = this.kerberos ? "dummyPwd" : this.pass;
    
        properties.put("mail.smtp.ssl.trust", this.host);
        properties.put("mail.smtp.host", this.host);
        properties.put("mail.smtp.user", username);
        properties.put("mail.smtp.password", password);
        properties.put("mail.smtp.port", this.port);
        if (this.authEnabled) {
            properties.put("mail.smtp.auth", "true");
        }
        properties.put("mail.smtp.connectiontimeout", "300000");
        properties.put("mail.smtp.timeout", "301000");
        if (this.kerberos) {
            properties.put("mail.smtp.starttls.enable", "true");
            properties.put("mail.smtp.starttls.required", "false");
            properties.put("mail.smtp.auth.login.disable", "true");
            properties.put("mail.smtp.auth.plain.disable", "true");
      
            properties.put("mail.smtp.sasl.enable", "true");
            properties.put("mail.smtp.sasl.mechanisms", "GSSAPI");
        }
        else if ((this.encrypt != null) && (this.encrypt.contains("TLS"))) {
            properties.put("mail.smtp.starttls.enable", "true");
            properties.put("mail.smtp.starttls.required", "true");
        }
        else if ((this.encrypt != null) && (this.encrypt.contains("SSL"))) {
            properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            properties.put("mail.smtp.socketFactory.fallback", "false");
            properties.put("mail.smtp.socketFactory.port", this.port);
            properties.put("mail.smtp.ssl.enable", "true");
        }
            properties.put("mail.debug", "true");
            properties.put("mail.smtp.connectionpool.debug", "true");
            properties.put("mail.smtp.sendpartial", "true");
        try {
            this.session = Session.getInstance(properties, new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            });
        } catch (Exception e) {
            throw new RuntimeException("Error creating mail session.", e);
        }
    
        this.session.setDebug(true);
        
    }  
    
    public void sendMessage(String toAddress, String mailSubject, String mailBody) throws MessagingException {
        
        if (this.session == null) {
            throw new RuntimeException("Unable to send message. Mail session not open.");
        }
        
        Message new_message = new MimeMessage(this.session);
        InternetAddress[] toAddresses = InternetAddress.parse(toAddress, false);
        new_message.setFrom(new InternetAddress(this.fromAddress));
        new_message.setRecipients(Message.RecipientType.TO, toAddresses);
        new_message.setSubject(mailSubject);
        BodyPart messagePart = new MimeBodyPart();
        messagePart.setContent(mailBody, "text/plain; charset=UTF-8");
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messagePart);
        new_message.setContent(multipart);
        
        if ((this.encrypt != null) && (this.encrypt.contains("SSL"))) {
            Transport transport = this.session.getTransport("smtps");
            transport.connect(this.host, this.port, this.user, this.pass);
            transport.sendMessage(new_message, new_message.getAllRecipients());
            transport.close();
        } else {
            Transport.send(new_message);
        }
        
    }
       
    public void ReadConfFromXMLFile() throws ParserConfigurationException, IOException, SAXException {
    
        String path = "E:" + File.separator + "3DActimize Confs" + File.separator + "email_confs.xml";
        
        File file = new File(path);
        
        if(!file.exists()) {
            
            try {
                
                file.getParentFile().mkdirs();
                
                DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
                
                DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

                Document document = documentBuilder.newDocument();
                
                Element root, 
                        smtp_host_comment_element, 
                        smtp_host, 
                        smtp_user_comment_element,
                        smtp_user,
                        smtp_user_password_comment_element,
                        smtp_user_password,
                        smtp_port_comment_element,
                        smtp_port,
                        smtp_auth_comment_element,
                        smtp_auth,
                        encryption_comment_element,
                        encryption,
                        from_email_comment_element,
                        from_email;
                
                Comment smtp_host_comment, 
                        smtp_user_comment,
                        smtp_user_password_comment,
                        smtp_port_comment,
                        smtp_auth_comment,
                        encryption_comment,
                        from_email_comment,
                        commentThree;
                
                root = document.createElement(CONFIGURATION);
                document.appendChild(root);
                
                //--------------------SMTP HOST---------------------//
                smtp_host_comment_element = document.getDocumentElement();
                smtp_host_comment = document.createComment(SMPT_HOST_COMMENT);
                smtp_host_comment_element.appendChild(smtp_host_comment);
                
                smtp_host = document.createElement(SMPT_HOST);
                smtp_host.appendChild(document.createTextNode("192.168.111.80"));
                root.appendChild(smtp_host);
                
                //------------------SMTP USER---------------------//
                smtp_user_comment_element = document.getDocumentElement();
                smtp_user_comment = document.createComment(USER_NAME_COMMENT);
                smtp_user_comment_element.appendChild(smtp_user_comment);
                
                smtp_user = document.createElement(USER_NAME);
                smtp_user.appendChild(document.createTextNode("niceactimize@pashabank.az"));
                root.appendChild(smtp_user);
                //----------------------------------------------//
                
                //------------------SMTP USER PASSWORD---------------------//
                smtp_user_password_comment_element = document.getDocumentElement();
                smtp_user_password_comment = document.createComment(PASSWORD_COMMENT);
                smtp_user_password_comment_element.appendChild(smtp_user_password_comment);
                
                smtp_user_password = document.createElement(PASSWORD);
                smtp_user_password.appendChild(document.createTextNode("N3e2up#res%dom1"));
                root.appendChild(smtp_user_password);
                //----------------------------------------------//
                
                //------------------SMTP PORT NUMBER---------------------//
                smtp_port_comment_element = document.getDocumentElement();
                smtp_port_comment = document.createComment(PORT_NUMBER_COMMENT);
                smtp_port_comment_element.appendChild(smtp_port_comment);
                
                smtp_port = document.createElement(PORT_NUMBER);
                smtp_port.appendChild(document.createTextNode("25"));
                root.appendChild(smtp_port);
                //----------------------------------------------//
                
                //------------------SMTP AUTH ENEBLED?---------------------//
                smtp_auth_comment_element = document.getDocumentElement();
                smtp_auth_comment = document.createComment(IS_SMPT_AUTH_ENEBLED_COMMENT);
                smtp_auth_comment_element.appendChild(smtp_auth_comment);
                
                smtp_auth = document.createElement(IS_SMPT_AUTH_ENEBLED);
                smtp_auth.appendChild(document.createTextNode("true"));
                root.appendChild(smtp_auth);
                //----------------------------------------------//
                
                //------------------ENCRYPTION--------------------//
                encryption_comment_element = document.getDocumentElement();
                encryption_comment = document.createComment(ENCRYPTION_COMMENT);
                encryption_comment_element.appendChild(encryption_comment);
                
                encryption = document.createElement(ENCRYPTION);
                encryption.appendChild(document.createTextNode("TLS"));
                root.appendChild(encryption);
                //----------------------------------------------//
                
                //------------------FROM EMAIL ADDRESS--------------------//
                from_email_comment_element = document.getDocumentElement();
                from_email_comment = document.createComment(FROM_ADDESS_COMMENT);
                from_email_comment_element.appendChild(from_email_comment);
                
                from_email = document.createElement(FROM_ADDESS);
                from_email.appendChild(document.createTextNode("niceactimize@pashabank.az"));
                root.appendChild(from_email);
                //----------------------------------------------//
                
                TransformerFactory transformerFactory = TransformerFactory.newInstance();
            
                Transformer transformer = transformerFactory.newTransformer();
                transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            
                DOMSource domSource = new DOMSource(document);
                StreamResult streamResult = new StreamResult(new File(path));
                
                transformer.transform(domSource, streamResult);

            } catch (TransformerConfigurationException ex) {
                Logger.getLogger(SmtpMail.class.getName()).log(Level.SEVERE, null, ex);
            } catch (TransformerException ex) {
                Logger.getLogger(SmtpMail.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = (Document) documentBuilder.parse(file);

        host = document.getElementsByTagName(SMPT_HOST).item(0).getTextContent();
        port = Integer.valueOf(document.getElementsByTagName(PORT_NUMBER).item(0).getTextContent());
        user = document.getElementsByTagName(USER_NAME).item(0).getTextContent();
        pass = document.getElementsByTagName(PASSWORD).item(0).getTextContent();
        authEnabled = Boolean.valueOf(document.getElementsByTagName(IS_SMPT_AUTH_ENEBLED).item(0).getTextContent());
        encrypt = document.getElementsByTagName(ENCRYPTION).item(0).getTextContent();
        fromAddress = document.getElementsByTagName(FROM_ADDESS).item(0).getTextContent();
        
    }
    
    public String SendAlertMails(String toAddresses, String mailSubject, String mailBody) {
        
        try {
        
            ReadConfFromXMLFile();
            createSession();
            sendMessage(toAddresses, mailSubject, mailBody);
            return "true";
        
        } catch (IOException ex) {
            return "Error while sending mails IOException: " + ex.toString();
        } catch (MessagingException ex) {
            return "Error while sending mails MessagingException: " + ex.toString();
        } catch (ParserConfigurationException ex) {
            return "Error while sending mails ParserConfigurationException: " + ex.toString();
        } catch (SAXException ex) {
            return "Error while sending mails SAXException: " + ex.toString();
        }
        
    }
    
}
